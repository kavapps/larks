package com.kavapps.larks.EventBus;

import com.kavapps.larks.Model.CategoryModel;
import com.kavapps.larks.Model.FoodModel;

public class FoodItemClick {

    private boolean success;
    private FoodModel categoryModel;

    public FoodItemClick(boolean success, FoodModel categoryModel) {
        this.success = success;
        this.categoryModel = categoryModel;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public FoodModel getCategoryModel() {
        return categoryModel;
    }

    public void setCategoryModel(FoodModel categoryModel) {
        this.categoryModel = categoryModel;
    }
}
