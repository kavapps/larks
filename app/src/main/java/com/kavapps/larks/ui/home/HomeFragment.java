package com.kavapps.larks.ui.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import com.asksira.loopingviewpager.LoopingViewPager;
import com.kavapps.larks.Adapter.MyBestDealsAdapter;
import com.kavapps.larks.Adapter.MyPopularCategoriesAdapter;
import com.kavapps.larks.R;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;

    Unbinder unbinder;

    @BindView(R.id.recyclerPopular)
    RecyclerView recyclerPopular;

    @BindView(R.id.loopingViewPager)
    LoopingViewPager loopingViewPager;

    LayoutAnimationController layoutAnimationController;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        unbinder = ButterKnife.bind(this,root);

        init();
        homeViewModel.getPopularList().observe(getViewLifecycleOwner(), popularCategoryModels -> {
            //Create Adapter
            MyPopularCategoriesAdapter  adapter = new MyPopularCategoriesAdapter(getContext(),popularCategoryModels);
            recyclerPopular.setAdapter(adapter);
            recyclerPopular.setLayoutAnimation(layoutAnimationController);
        });


        homeViewModel.getBestDealList().observe(getViewLifecycleOwner(), bestDealModels -> {
            //Create Adapter
            MyBestDealsAdapter adapter = new MyBestDealsAdapter(getContext(),bestDealModels,true);
            loopingViewPager.setAdapter(adapter);
        });
        return root;
    }

    private void init() {
        layoutAnimationController = AnimationUtils.loadLayoutAnimation(getContext(),R.anim.layout_item_from_left);
        recyclerPopular.setHasFixedSize(true);
        recyclerPopular.setLayoutManager(new LinearLayoutManager(getContext(),RecyclerView.HORIZONTAL,false));
    }

    @Override
    public void onResume() {
        super.onResume();
        loopingViewPager.resumeAutoScroll();
    }

    @Override
    public void onPause() {
        loopingViewPager.pauseAutoScroll();
        super.onPause();
    }
}