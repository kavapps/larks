package com.kavapps.larks.ui.menu;


import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.kavapps.larks.Callback.ICategoryCallBackListener;
import com.kavapps.larks.Common.Common;
import com.kavapps.larks.Model.BestDealModel;
import com.kavapps.larks.Model.CategoryModel;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class MenuViewModel extends ViewModel implements ICategoryCallBackListener {


    private MutableLiveData<List<CategoryModel>> categoryListMutable;
    private MutableLiveData<String> messageError = new MutableLiveData<>();
    private ICategoryCallBackListener categoryCallBackListener;


    public MenuViewModel() {
        categoryCallBackListener = this;
    }


    public MutableLiveData<List<CategoryModel>> getCategoryListMutable() {
        if (categoryListMutable ==null){
            categoryListMutable = new MutableLiveData<>();
            messageError = new MutableLiveData<>();
            loadCategories();
        }
        return categoryListMutable;
    }

    private void loadCategories() {

        List<CategoryModel> tmpLIst = new ArrayList<>();
        DatabaseReference categoryRef = FirebaseDatabase.getInstance().getReference(Common.CATEGORY_REF);

        categoryRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for(DataSnapshot itemSnapshot:snapshot.getChildren()){
                    CategoryModel  model = itemSnapshot.getValue(CategoryModel.class);
                    model.setMenuId(itemSnapshot.getKey());
                    tmpLIst.add(model);
                }
                categoryCallBackListener.onCategoryLoadSuccess(tmpLIst);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                categoryCallBackListener.onCategoryLoadFailed(error.getMessage());
            }
        });
    }

    public MutableLiveData<String> getMessageError() {
        return messageError;
    }

    @Override
    public void onCategoryLoadSuccess(List<CategoryModel> categoryModelList) {
        categoryListMutable.setValue(categoryModelList);
    }

    @Override
    public void onCategoryLoadFailed(String message) {
        messageError.setValue(message);
    }
}