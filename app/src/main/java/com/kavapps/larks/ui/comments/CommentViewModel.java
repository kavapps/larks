package com.kavapps.larks.ui.comments;

import com.kavapps.larks.Model.CommentModel;
import com.kavapps.larks.Model.FoodModel;

import java.util.List;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class CommentViewModel extends ViewModel {
    private MutableLiveData<List<CommentModel>> mutableLiveDataCommentsList;

    public CommentViewModel() {
        mutableLiveDataCommentsList = new MutableLiveData<>();
    }

    public MutableLiveData<List<CommentModel>> getMutableLiveDataFoodList() {
        return mutableLiveDataCommentsList;
    }

    public void setCommentsList(List<CommentModel> commentsList){
        mutableLiveDataCommentsList.setValue(commentsList);
    }
}
