package com.kavapps.larks.ui.fooddetail;

import android.util.Log;

import com.kavapps.larks.Common.Common;
import com.kavapps.larks.Model.CommentModel;
import com.kavapps.larks.Model.FoodModel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class FoodDetailViewModel extends ViewModel {

    private MutableLiveData<FoodModel> mutableLiveDataFood;
    private MutableLiveData<CommentModel> mutableLiveDataComment;

    public void setFoodModel(FoodModel foodModel) {
        if (mutableLiveDataFood!=null)
        mutableLiveDataFood.setValue(foodModel);
    }


    public void setCommentModel(CommentModel commentModel) {
        if (mutableLiveDataComment == null)
            Log.e("kav","setCommentModel = "+commentModel.getComment());
            mutableLiveDataComment.setValue(commentModel);

    }

    public MutableLiveData<CommentModel> getMutableLiveDataComment() {
        return mutableLiveDataComment;
    }

    public FoodDetailViewModel() {
        mutableLiveDataComment = new MutableLiveData<>();
    }

    public MutableLiveData<FoodModel> getMutableLiveDataFood() {
        if (mutableLiveDataFood == null)
            mutableLiveDataFood = new MutableLiveData<>();
        mutableLiveDataFood.setValue(Common.selectedFood);
        return mutableLiveDataFood;
    }
}