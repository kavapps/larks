package com.kavapps.larks.Adapter;

import android.content.Context;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.android.gms.common.util.DataUtils;
import com.kavapps.larks.Model.CommentModel;
import com.kavapps.larks.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class MyCommentsAdapter extends RecyclerView.Adapter<MyCommentsAdapter.MyViewHolder> {

    Context context;
    List<CommentModel> commentModelList;

    public MyCommentsAdapter(Context context, List<CommentModel> commentModelList) {
        this.context = context;
        this.commentModelList = commentModelList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.layout_comment_item,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Long timeStamp = Long.valueOf(commentModelList.get(position).getCommentTimeStamp().get("timeStamp").toString());
        holder.textCommentDate.setText(DateUtils.getRelativeTimeSpanString(timeStamp));
        holder.textComment.setText(commentModelList.get(position).getComment());
        holder.textCommentName.setText(commentModelList.get(position).getName());
        holder.ratingBar.setRating(commentModelList.get(position).getRatingValue());
    }

    @Override
    public int getItemCount() {
        return commentModelList.size();
    }

    public class MyViewHolder  extends RecyclerView.ViewHolder{

        private Unbinder unbinder;
        @BindView(R.id.textCommentDate)
        TextView textCommentDate;

        @BindView(R.id.textComment)
        TextView textComment;

        @BindView(R.id.textCommentName)
        TextView textCommentName;

        @BindView(R.id.ratingBar)
        RatingBar ratingBar;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            unbinder = ButterKnife.bind(this,itemView);
        }
    }
}
