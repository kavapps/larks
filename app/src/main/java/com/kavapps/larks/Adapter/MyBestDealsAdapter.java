package com.kavapps.larks.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.asksira.loopingviewpager.LoopingPagerAdapter;
import com.bumptech.glide.Glide;
import com.kavapps.larks.Model.BestDealModel;
import com.kavapps.larks.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class MyBestDealsAdapter extends LoopingPagerAdapter<BestDealModel> {

    @BindView(R.id.imageBestDeal)
    ImageView imageBestDeal;

    @BindView(R.id.textBestDeal)
    TextView textBestDeal;

    Unbinder unbinder;
    Context context;
    List<BestDealModel> itemList;
    boolean isInfinite;


    public MyBestDealsAdapter(Context context, List<BestDealModel> itemList, boolean isInfinite) {
        super(context, itemList, isInfinite);
        this.context = context;
        this.itemList = itemList;
        this.isInfinite = isInfinite;
    }

    @Override
    protected void bindView(View view, int listPosition, int i1) {
        unbinder = ButterKnife.bind(this,view);
        //Set data

        Glide.with(view)
                .load(itemList.get(listPosition).getImage())
                .into(imageBestDeal);
        textBestDeal.setText(itemList.get(listPosition).getName());


    }

    @Override
    protected View inflateView(int i, ViewGroup viewGroup, int i1) {
        return LayoutInflater.from(context).inflate(R.layout.layout_best_deal_item,viewGroup,false);
    }
}
