package com.kavapps.larks.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.kavapps.larks.Callback.IRecyclerClickListener;
import com.kavapps.larks.Common.Common;
import com.kavapps.larks.EventBus.CategoryClick;
import com.kavapps.larks.Model.CategoryModel;
import com.kavapps.larks.R;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class MyCategoriesAdapter extends RecyclerView.Adapter<MyCategoriesAdapter.MyViewHolder> {

    Context context;
    List<CategoryModel> categoryModelList;

    public MyCategoriesAdapter(Context context, List<CategoryModel> categoryModelList) {
        this.context = context;
        this.categoryModelList = categoryModelList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.layout_category_item,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Glide.with(context)
                .load(categoryModelList.get(position).getImage())
                .into(holder.imageCategory);
        holder.textCategory.setText(new StringBuffer(categoryModelList.get(position).getName()));

        //Event
        holder.setListener((view, pos) -> {
            Common.categorySelected = categoryModelList.get(pos);
            EventBus.getDefault().postSticky(new CategoryClick(true,categoryModelList.get(pos)));
        });
    }

    @Override
    public int getItemCount() {
        return categoryModelList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        Unbinder unbinder;

        @BindView(R.id.imageCategory)
        ImageView imageCategory;

        @BindView(R.id.textCategory)
        TextView textCategory;

        IRecyclerClickListener listener;

        public MyViewHolder(@NonNull View itemView, IRecyclerClickListener listener) {
            super(itemView);
            this.listener = listener;
        }

        public void setListener(IRecyclerClickListener listener) {
            this.listener = listener;
        }

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            unbinder = ButterKnife.bind(this,itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            listener.onItemClickListener(view,getAdapterPosition());
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (categoryModelList.size() ==1){
            return Common.DEFAULT_COLUMN_COUNT;
        }else {
            if (categoryModelList.size()%2==0){
                return Common.DEFAULT_COLUMN_COUNT;
            }else {
                return (position >1 &&position==categoryModelList.size()-1)?Common.FULL_WIDTH_COLUMN:Common.DEFAULT_COLUMN_COUNT;
            }
        }
    }
}
