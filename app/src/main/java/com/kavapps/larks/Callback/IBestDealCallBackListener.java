package com.kavapps.larks.Callback;

import com.kavapps.larks.Model.BestDealModel;

import java.util.List;

public interface IBestDealCallBackListener {
    void onBestDealLoadSuccess(List<BestDealModel> bestDealModels);
    void onBestDealLoadFailed(String message);
}
