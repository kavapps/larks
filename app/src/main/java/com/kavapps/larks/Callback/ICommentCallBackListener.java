package com.kavapps.larks.Callback;

import com.kavapps.larks.Model.CommentModel;

import java.util.List;

public interface ICommentCallBackListener {

    void onCommentLoadSuccess(List<CommentModel> commentModels);
    void onCommentLoadFailed(String message);
}
