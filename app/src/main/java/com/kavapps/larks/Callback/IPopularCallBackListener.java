package com.kavapps.larks.Callback;

import com.kavapps.larks.Model.PopularCategoryModel;

import java.util.List;

public interface IPopularCallBackListener {
    void onPopularLoadSuccess(List<PopularCategoryModel> popularCategoryModels);
    void onPopularLoadFailed(String message);

}
